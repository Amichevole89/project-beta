from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import( 
    TechnicianEncoder,
    AppointmentListEncoder,
    AppointmentDetailEncoder,
)
from .models import Technician, Appointment, AutomobileVO

@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {'Error': "Invalid Technician"}
            )

@require_http_methods(['GET', 'POST'])  
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.get(
            id=content['technician']
        )  
        content['technician'] = technician
        try:
            AutomobileVO.objects.get(vin=content['vin'])
            content['VIP_treatment'] = True
        except AutomobileVO.DoesNotExist:
            content['VIP_treatment'] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment, 
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
@require_http_methods(['GET', 'PUT', 'DELETE'])
def appointment_detail(request, pk):
    if request.method == 'GET':
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if 'technician' in content:
                technician = Technician.objects.get(
                    id=content['technician']
                )
                content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {'Error': 'Invalid Technician, please select one that exists'}
            )
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {'Appointment Deleted': count > 0},
        )

