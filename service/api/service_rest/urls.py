from django.urls import path

from .views import (list_appointments, appointment_detail, list_technicians)

urlpatterns = [
    path('appointments/', list_appointments, name='list_appointments'),
    path('appointments/<int:pk>/', appointment_detail, name='appointment_detail'),
    path('technicians/', list_technicians, name='list_technicians'),
]
