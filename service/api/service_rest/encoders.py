from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['import_href', 'vin']

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ['name', 'employee_number', 'id',]

class AppointmentListEncoder(ModelEncoder):
    model=Appointment
    properties = [
      
        'technician',
        'customer_name', 
        'VIP_treatment', 
        'vin',
        'date',
        'time',
        'purpose',
        'completed',
        'id',
        ]
    encoders = {
        'technician': TechnicianEncoder(),
        'automobile': AutomobileVOEncoder(),
    }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'technician',
        'customer_name',
        'VIP_treatment',
        'date',
        'time',
        'vin',
        'purpose',
        'completed',
    ]
    encoders = {
        'technician': TechnicianEncoder()
    }