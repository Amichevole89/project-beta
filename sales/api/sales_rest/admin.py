from django.contrib import admin
from .models import (AutoMobileVO, Customer, SalesPerson, SaleRecord)
# Register your models here.

@admin.register(AutoMobileVO)
class AutoMobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class SaleRecordAdmin(admin.ModelAdmin):
    pass