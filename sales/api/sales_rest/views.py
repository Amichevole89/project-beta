from django.shortcuts import render
import json

from .models import (AutoMobileVO, Customer, SalesPerson, SaleRecord)
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = [
        "picture_url",
        "vin"
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["automobile", "price", "salesperson", "customer", "id"]

    encoders = {
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson,},
            encoder=SalesPersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_salerecords(request, salesperson_id=None):
    if request.method == "GET":
        if salesperson_id is not None:
            salerecords = SaleRecord.objects.filter(salesperson=salesperson_id)
        else:
            salerecords = SaleRecord.objects.all()
        return JsonResponse(
            {"salerecords": salerecords},
            encoder=SaleRecordEncoder,
            safe=False,
            )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            name = content["customer"]
            customer = Customer.objects.get(name=name)
            print(customer)
            content['customer'] = customer

            salespersonname = content['salesperson']
            salesperson = SalesPerson.objects.get(id=salespersonname)
            print(salesperson)
            content["salesperson"] = salesperson

            cat = content["automobile"]
            automobile = AutoMobileVO.objects.get(vin=cat)
            content["automobile"] = automobile

            salerecord = SaleRecord.objects.create(**content)
            return JsonResponse(
                salerecord,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except Exception as e:
            print(e)
            return JsonResponse(
                {"message": "Automobile Does Not Exist"},
                status = 400,
            )

