from django.urls import path
from .views import api_list_customers, api_list_salerecords, api_list_salesperson

urlpatterns = [
    path("salesperson/", api_list_salesperson, name="api_list_salesperson"),
    path("salesrecord/", api_list_salerecords, name="api_list_salerecords"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salesperson/<int:salesperson_id>/salesrecord/", api_list_salerecords, name="api_filter_sales")
]