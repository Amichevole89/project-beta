from django.db import models

# Create your models here.
class AutoMobileVO(models.Model):
    model = models.CharField(max_length=100)
    picture_url = models.URLField()
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField(null=True)
    vin = models.CharField(max_length=17, unique=True)

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.name

class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutoMobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.PROTECT,
    )
    price = models.CharField(max_length=30)
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT,
    )