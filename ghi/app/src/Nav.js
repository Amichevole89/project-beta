import { NavLink } from "react-router-dom";
import ServiceNav from "./Components/Services/ServiceNav";
import InventoryNav from "./Components/Inventory/InventoryNav";
import SalesNav from "./Components/Sales/SalesNav";

function Nav() {
  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-info justify-content'>
      <div className='container-fluid'>
        <div className='d-grid gap-2 d-md-flex justify-content-md-start'>
          <NavLink className='navbar-brand' to='/'>
            CarCar
          </NavLink>
          <InventoryNav />
          <SalesNav />
          <ServiceNav />
        </div>
      </div>
    </nav>
  );
}

export default Nav;
