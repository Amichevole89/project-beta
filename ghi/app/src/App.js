import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./Components/Services/TechnicianForm";
import AppointmentForm from "./Components/Services/AppointmentForm";
import AppointmentList from "./Components/Services/AppointmentList";
import AppointmentHistory from "./Components/Services/AppointmentHistory";
import SalesPersonForm from "./Components/Sales/SalesPersonForm";
import SalesRecordForm from "./Components/Sales/SalesRecordForm";
import CustomerForm from "./Components/Sales/CustomerForm";
import SalesRecordList from "./Components/Sales/SalesRecordList";
import SalesHistory from "./Components/Sales/SalesHistory";
import ManufacturerForm from "./Components/Inventory/ManufacturerForm";
import ManufacturerList from "./Components/Inventory/ManufacturerList";
import AutomobilesList from "./Components/Inventory/AutomobilesList";
import AutomobileForm from "./Components/Inventory/AutomobileForm";
import VehicleForm from './Components/Inventory/VehicleForm';
import VehicleList from "./Components/Inventory/VehicleList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className='container'>
        <Routes>
          <Route path='/' element={<MainPage />} />
          <Route path='service/technicians/new/' element={<TechnicianForm />} />
          <Route path='service/appointments/' element={<AppointmentList />} />
          <Route path='service/appointments/new/' element={<AppointmentForm />} />
          <Route path='service/appointments/history/' element={<AppointmentHistory/>}/>
          <Route path='salesperson/new' element={<SalesPersonForm />} />
          <Route path='salesrecord/new' element={<SalesRecordForm />} />
          <Route path='customers/new' element={<CustomerForm />} />
          <Route path='salesrecord/' element={<SalesRecordList />} />
          <Route path='saleshistory/' element={<SalesHistory />} />
          <Route path='manufacturer/new' element={<ManufacturerForm />} />
          <Route path='manufacturer' element={<ManufacturerList />} />
          <Route path='automobiles/' element={<AutomobilesList/>}/>
          <Route path='automobiles/new/'element={<AutomobileForm/>}/> 
          <Route path='vehicles/new/' element={<VehicleForm/>}/>
          <Route path='vehicles' element={<VehicleList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
