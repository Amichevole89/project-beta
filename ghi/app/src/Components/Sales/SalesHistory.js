import React from 'react';


class SalesHistory extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        salesperson: [],
        salerecords: [],
    }
  this.getSalesPerson = this.getSalesPerson.bind(this);
  this.getSaleRecData = this.getSaleRecData.bind(this);
  this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
  };
  async handleSalesPersonChange(event) {
    const value = event.target.value
    const getsalesURL = `http://localhost:8090/api/salesperson/${value}/salesrecord/`
    const getsaleresponse = await fetch(getsalesURL);
    const data = await getsaleresponse.json()
    this.setState({salerecords:data.salerecords})
  }

  async getSalesPerson() {
    const salespersonURL = "http://localhost:8090/api/salesperson/";
    const salespersonresponse = await fetch(salespersonURL);
    if (salespersonresponse.ok) {
        const salesData = await salespersonresponse.json();
        this.setState({salesperson: salesData.salesperson})
    }
  }

  async getSaleRecData() {
    const url = 'http://localhost:8090/api/salesrecord/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ salerecords: data.salerecords})
    }
  }

  async componentDidMount() {
    this.getSaleRecData()
    this.getSalesPerson()
  }

  render() {
    return (
        <>
        <div className="form-floating mb-3">
            <select onChange={this.handleSalesPersonChange} value={this.state.salesperson.salesperson} name="salesperson" id="salesperson" className="form-select">
                <option value="">Select salesperson</option>
                {this.state.salesperson.map(saleperson => {
                    return (
                        <option key={saleperson.id} value={saleperson.id}>
                            {saleperson.name}
                        </option>
                    )
                })}
            </select>
        </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody> 
            {this.state.salerecords.map((salerecord) => {
            return (
              <tr key={salerecord.id}>
                <td>{ salerecord.salesperson.name }</td>
                <td>{ salerecord.salesperson.employee_number }</td>
                <td>{ salerecord.customer.name }</td>
                <td>{ salerecord.automobile.vin }</td>
                <td>{ salerecord.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }
}

export default SalesHistory;


