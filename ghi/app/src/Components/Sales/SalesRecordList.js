import React from 'react';


class SalesRecordList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salerecords: [],
    }
  };

  async getSaleRecData() {
    const url = 'http://localhost:8090/api/salesrecord/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ salerecords: data.salerecords})
    }
  }

  async componentDidMount() {
    this.getSaleRecData()
  }


  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.salerecords.map(salerecord => {
            return (
              <tr key={salerecord.id}>
                <td>{ salerecord.salesperson.name }</td>
                <td>{ salerecord.salesperson.employee_number }</td>
                <td>{ salerecord.customer.name }</td>
                <td>{ salerecord.automobile.vin }</td>
                <td>{ salerecord.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default SalesRecordList;
