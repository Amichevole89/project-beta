import React from 'react'


class CreateSalesRecord extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      salespersons: [],
      customers: [],
      price: '', 
      autos: [], 
    }
    this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
    this.handleCustomerChange = this.handleCustomerChange.bind(this)
    this.handlePriceChange = this.handlePriceChange.bind(this)
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.autos;
    delete data.customers;
    delete data.salespersons;
    console.log(data)
    const salesrecordURL = 'http://localhost:8090/api/salesrecord/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesrecordURL, fetchConfig);
    if (response.ok) {
      const cleared = {
        salesperson: '',
        customer: '',
        price: '', 
        automobile: '', 
      }
      this.setState(cleared)
    }
  }

  async componentDidMount() {
    const salespersonURL = 'http://localhost:8090/api/salesperson/';
    const salespersonresponse = await fetch(salespersonURL);
    if (salespersonresponse.ok) {
      const salespersondata = await salespersonresponse.json();
      this.setState({salespersons: salespersondata.salesperson});
    }

    const customerURL = 'http://localhost:8090/api/customers/';
    const customerresponse = await fetch(customerURL);
    if (customerresponse.ok) {
      const customerdata = await customerresponse.json();
      this.setState({customers: customerdata.customer});
    }

    const autoURL = 'http://localhost:8100/api/automobiles/';
    const autoResponse = await fetch(autoURL);
    if (customerresponse.ok) {
      const autoData = await autoResponse.json();
      this.setState({autos: autoData.autos});
    }

  }



  handleSalesPersonChange(event) {
    const value = event.target.value
    this.setState({salesperson:value})
  }

  handleCustomerChange(event) {
    const value = event.target.value
    this.setState({customer:value})
  }

  handlePriceChange(event) {
    const value = event.target.value
    this.setState({price:value})
  }

  handleAutomobileChange(event) {
    const value = event.target.value
    this.setState({automobile:value})
  }


  render () {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create sales record</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
            <select onChange={this.handleSalesPersonChange} required name="salesperson" id="salesperson" value={this.state.salesperson} className="form-select">
            <option value="">Choose a sales person</option>
            {this.state.salespersons.map(salesperson => {
            return (
                <option key={salesperson.id} value={salesperson.id}>
                {salesperson.name}
                </option>
            );
            })}
            </select>
            </div>
            <div className="form-floating mb-3">
            <select onChange={this.handleCustomerChange} required name="customer" id="customer" value={this.state.customer} className="form-select">
            <option value="">Choose a customer</option>
            {this.state.customers.map((customer) => {
            return (
                <option key={customer.name} value={customer.name}>
                {customer.name}
                </option>
            );
            })}
            </select>
            </div>
            <div className="form-floating mb-3">
            <input onChange={this.handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" value={this.state.price} />
            <label htmlFor="style">Price</label>
            </div>
            <div className="form-floating mb-3">
            <select onChange={this.handleAutomobileChange} required name="automobile" id="automobile" value={this.state.automobile} className="form-select">
            <option value="">Choose an automobile</option>
            {this.state.autos.map(automobile => {
            return (
                <option key={automobile.vin} value={automobile.vin}>
                {automobile.vin}
                </option>
            );
            })}
            </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default CreateSalesRecord
