import React from 'react'
import {NavLink} from 'react-router-dom'

const SalesNav = () => {
  return (
    <div className='p-2 dropdown'>
            <NavLink
              className='link-info btn btn-secondary dropdown-toggle'
              to='#'
              role='button'
              id='dropdownMenu'
              data-bs-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            >
              Sales
            </NavLink>
            <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/saleshistory'>
                Sales search
              </NavLink>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/salesrecord'>
                List of sales
              </NavLink>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/salesrecord/new'>
                Create sales record
              </NavLink>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/salesperson/new'>
                Add salesperson
              </NavLink>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/customers/new'>
                Add customer
              </NavLink>
            </div>
          </div>
  )
}

export default SalesNav