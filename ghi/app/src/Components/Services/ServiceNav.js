import React from "react";
import { NavLink } from "react-router-dom";

const ServiceNav = () => {
  return (
    <div className='p-2 dropdown'>
      <NavLink

        className='link-info btn btn-secondary dropdown-toggle '
        // style={({ isActive }) => ({color: isActive ? 'black' : 'info'}, {bg: isActive ? '#6c64b3' : '#6c64b3' })}
        to='#'
        role='button'
        id='dropdownMenu'
        data-bs-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'
        
      >
        Services
      </NavLink>

      <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
        <NavLink className='dropdown-item' to='/service/appointments/'>
          List of appointments
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/service/appointments/new/'>
          Enter a service appointment
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/service/technicians/new/'>
          Enter a technician
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/service/appointments/history/'>
          Appointment History
        </NavLink>
      </div>
    </div>
  );
};

export default ServiceNav;
