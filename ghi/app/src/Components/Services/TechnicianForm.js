import React, { useState, useEffect } from "react";
import {Link} from 'react-router-dom'

const TechnicianForm = () => {
  const [technician, setTechnician] = useState({
    name: "",
    employee_number: "",
    technicians: [],
  });

  const techniciansUrl = "http://localhost:8080/api/technicians/";

  const handleInputChange = (e) => {
    setTechnician({ ...technician, [e.target.name]: e.target.value });
  };

  const fetchData = async () => {
    try {
      const response = await fetch(techniciansUrl);
      if (response.ok) {
        const data = await response.json();

        setTechnician((s) => ({ ...s, technicians: data.technicians }));
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...technician };
    delete content.technicians;
    await fetch(`http://localhost:8080/api/technicians/`, {
      method: "POST",
      body: JSON.stringify(content),
      headers: { "Content-Type": "application/json" },
    });
    setSubmitSuccess(true);
  };
  const [submitSuccess, setSubmitSuccess] = useState(false);

  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (submitSuccess) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1> Enter a Technician</h1>
            <form onSubmit={handleSubmit} className={formClass}>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={technician.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={technician.employee_number}
                  placeholder='Employee Number'
                  required
                  type='number'
                  name='employee_number'
                  id='employee_number'
                  className='form-control'
                />
                <label htmlFor='name'>Employee Number</label>
              </div>

              <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created new technician!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSubmitSuccess(false)}
                  className='btn btn-outline-warning  '
                >
                  Create another technician?
                </button>
                <Link className='btn btn-outline-primary' to='/service/appointments/'>
                  Return to Appointments
                </Link>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TechnicianForm;
