import React, { useEffect, useState } from "react";

const AppointmentHistory = () => {
  const [appointments, setAppointments] = useState([]);
  const [filterAppts, setFilterAppts] = useState([]);
  const [input, setInput] = useState("");

  const url = "http://localhost:8080/api/appointments/";

  const fetchTrueAppts = async () => {
    const response = await fetch(url);
    if (response.ok) {
      const allAppointments = await response.json();
      const setMultipleStates = allAppointments.appointments.filter(
        (appt) => appt.completed === true
      );
      setAppointments(setMultipleStates);
      setFilterAppts(setMultipleStates);
    }
  };

  const filtering = appointments.filter(
    (appointment) => appointment.vin === input
  );

  const handleInputChange = (e) => {
    e.preventDefault();
    setInput(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    input ? setFilterAppts(filtering) : setFilterAppts(appointments);
  };

  useEffect(() => {
    fetchTrueAppts();
  }, []);

  return (
    <div className='container-fluid'>
      <h1 className='text-center p-2 m-2'>Appointment list history</h1>
      <form className='form-fluid' onSubmit={handleSubmit}>
        <div className='input-group mb-3'>
          <input
            onChange={handleInputChange}
            placeholder='Enter VIN Number'
            value={appointments.vin}
            type='search'
            name='vin'
            id='vin'
            className='form-control'
            aria-describedby='searchInput'
          />
          <button
            className='btn btn-outline-secondary'
            type='search'
            data-bs-toggle='searchInput'
          >
            Search
          </button>
        </div>
      </form>
      <div className='d-grid gap-2 d-md-flex justify-content-md-between'></div>
      <table className='table table-hover '>
        <thead>
          <tr>
            <th>Vin</th>
            <th>Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {filterAppts.map((appt) => (
            <tr key={appt.id}>
              <td>{appt.vin}</td>
              <td>{appt.customer_name}</td>
              <td>{appt.date}</td>
              <td>{appt.time}</td>
              <td>{appt.technician.name}</td>
              <td>{appt.purpose}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default AppointmentHistory;
