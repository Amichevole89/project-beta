import React, { useEffect, useState } from "react";

const AppointmentList = () => {
  const [appointments, setAppointments] = useState([]);
  const url = "http://localhost:8080/api/appointments/";

  const fetchFalseAppts = async () => {
    const response = await fetch(url);
    if (response.ok) {
      const allAppointments = await response.json();
      setAppointments(
        allAppointments.appointments.filter((appt) => appt.completed === false)
      );
    }
  };

  useEffect(() => {
    fetchFalseAppts();
  }, []);

  const deleteAppointment = async (grape) => {
    await fetch(`${url}${grape}/`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    fetchFalseAppts(setAppointments);
  };

  const finishAppointment = async (id) => {
    const response = await fetch(`${url}${id}/`, {
      method: "PUT",
      body: JSON.stringify({ completed: true }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      fetchFalseAppts(setAppointments);
    }
  };

  const handleClick = (id) => {
    finishAppointment(id);
  };

  return (
    <div className='container-fluid'>
      <h1 className='text-center p-2 m-2'>Appointments</h1>
      <div className='d-grid gap-2 d-md-flex justify-content-md-between'></div>
        <table className='table table-hover '>
          <thead>
            <tr>
              <th>Vip</th>
              <th>Name</th>
              <th>Vin</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appt) => (
              <tr key={appt.id}>
                {appt.VIP_treatment ? <td className='text-success fw-bold'>True</td> : <td className='text-danger fw-bold'>False</td>}
                <td>{appt.customer_name}</td>
                <td>{appt.vin}</td>
                <td>{appt.date}</td>
                <td>{appt.time}</td>
                <td>{appt.technician.name}</td>
                <td>{appt.purpose}</td>
                <td className='d-grid gap-2 d-lg-flex justify-content-md-end'>
                  <button
                    onClick={async () => {
                      await deleteAppointment(appt.id);
                      await fetchFalseAppts();
                    }}
                    className='btn btn-outline-danger'
                  >
                    Cancel
                  </button>
                  <button
                    onClick={() => {
                      handleClick(appt.id);
                    }}
                    className='btn btn-outline-success'
                  >
                    Finished
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
    </div>
  );
};

export default AppointmentList;
