import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const AppointmentForm = () => {
  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";

  const [loading, setLoading] = useState(false);
  const [successSubmit, setSuccessSubmit] = useState(false);
  const [appointment, setAppointment] = useState({
    customer_name: "",
    VIP_treatment: "",
    vin: "",
    purpose: "",
    date: "",
    time: "",
    technician: "",
    technicians: [],
  });

  const fetchTechnicians = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      if (response.ok) {
        const content = await response.json();
        setLoading(false);
        setAppointment((s) => ({ ...s, technicians: content.technicians }));
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  useEffect(() => {
    setLoading(true);
    fetchTechnicians();
  }, []);

  const handleInputChange = (e) => {
    setAppointment({ ...appointment, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...appointment };
    delete content.technicians;
    const response = await fetch(`http://localhost:8080/api/appointments/`, {
      method: "POST",
      body: JSON.stringify(content),
      headers: { "Content-Type": "application/json" },
    });
    if (response.ok) {
      setSuccessSubmit(true);

    }
  };

  if (successSubmit) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      {loading && (
        <div>
          {""}
          <h1>Loading...</h1>
        </div>
      )}
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create Appointment</h1>
            <form onSubmit={handleSubmit} className={formClass}>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={appointment.customer_name}
                  placeholder='customer_name'
                  required
                  type='text'
                  name='customer_name'
                  id='customer_name'
                  className='form-control shadow-lg'
                />
                <label htmlFor='customer_name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={appointment.vin}
                  placeholder='Model'
                  required
                  type='text'
                  name='vin'
                  id='vin'
                  className='form-control  text-success shadow-lg'
                />
                <label htmlFor='vin'>Vin</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={appointment.purpose}
                  placeholder='purpose'
                  required
                  type='text'
                  name='purpose'
                  id='purpose'
                  className='form-control shadow-lg'
                />
                <label htmlFor='purpose'>purpose</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={appointment.date}
                  placeholder='purpose'
                  required
                  type='date'
                  name='date'
                  id='date'
                  className='form-control shadow-lg'
                />
                <label htmlFor='date'>date</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={appointment.time}
                  placeholder='time'
                  required
                  type='time'
                  name='time'
                  id='time'
                  className='form-control text-primary fw-bold shadow-lg'
                />
                <label htmlFor='time'>time</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={appointment.technician}
                  placeholder='Technician'
                  required
                  type='text'
                  name='technician'
                  id='technician'
                  className='form-control fw-bold text-warning shadow-lg'
                >
                  <option value=''>Choose a technician</option>
                  {appointment.technicians.map((technician) => {
                    return (
                      <option key={[technician.id]} value={[technician.id]}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-between'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created Appointment!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSuccessSubmit(false)}
                  className='btn btn-outline-warning'
                >
                  Create a second appointment?
                </button>
                <Link className='btn btn-outline-primary' to='/service/appointments/'>
                  Return to Appointments
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AppointmentForm;

