import React from "react";
import { NavLink } from "react-router-dom";

const InventoryNav = () => {
  return (
    <div className='p-2 dropdown'>
      <NavLink
        className='link-info btn btn-secondary dropdown-toggle'
        to='#'
        role='button'
        id='dropdownMenu'
        data-bs-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'
      >
        Inventory
      </NavLink>

      <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
        <NavLink className='dropdown-item' to='/manufacturer/new'>
          Add Manufacturer
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/manufacturer'>
          List Manufacturers
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/vehicles'>
          List Vehicle Models
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/vehicles/new/'>
          Create Vehicle Model
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/automobiles'>
          List Automobiles
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/automobiles/new/'>
          Create Automobile
        </NavLink>
      </div>
    </div>
  );
};

export default InventoryNav;
