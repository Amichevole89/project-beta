import React, { useEffect, useState } from "react";

const url = "http://localhost:8100/api/automobiles/";

const AutomobilesList = () => {
  const [automobiles, setAutomobiles] = useState([]);

  const getAutomobiles = async () => {
    const response = await fetch(url);
    if (response.ok) {
      const allAutomobiles = await response.json();
      setAutomobiles(allAutomobiles.autos);
    }
  };

  useEffect(() => {
    getAutomobiles();
  }, []);

  return (
    <div className='container-fluid'>
            <h1 className='text-center'>Automobiles</h1>
      
    <table className='table table-hover '>
    
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Year</th>
          <th>Color</th>
          <th>Vin</th>
          <th>Picture</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map((car) => (
          <tr key={car.vin}>
            <td>{car.model.manufacturer.name}</td>
            <td>{car.model.name}</td>
            <td>{car.year}</td>
            <td>{car.color}</td>
            <td>{car.vin}</td>
            <td>
              <img src={car.model.picture_url} alt='invalid' height='100' />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
    </div>
  );
};

export default AutomobilesList;
