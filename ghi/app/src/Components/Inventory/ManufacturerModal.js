import React from "react";
import ManufacturerForm from "./ManufacturerForm";

const ManufacturerModal = () => {
  return (
    <div>
      <button
        type='button'
        id='shoeModal'
        data-bs-toggle='modal'
        className='btn btn-outline-success btn-md px-4 gap-3'
        data-bs-target='#modalManufacturer'
      >
        Create Manufacturer
      </button>
      <div
        className='modal fade'
        id='modalManufacturer'
        tabIndex='-1'
        aria-labelledby='manufacturerModalLabel'
        aria-hidden='true'
      >
        <div className='modal-dialog modal-lg'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='manufacturerModalLabel'>
                Please complete form to continue
              </h5>
              <button
                type='button'
                className='btn-close'
                data-bs-dismiss='modal'
                aria-label='Close'
              ></button>
            </div>
            <div className='modal-body'>
              <div>
                <ManufacturerForm />
              </div>
            </div>
            <div className='modal-footer'>
              <button
                data-bs-dismiss='modal'
                type='button'
                className='btn btn-outline-danger'
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ManufacturerModal;
