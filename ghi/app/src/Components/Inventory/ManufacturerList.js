import React from 'react';

class ManufacturerList extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        manufacturers: [],
      }
    };
    async getManufacturerData() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers })
        }
    }
    async componentDidMount() {
        this.getManufacturerData()
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer Names</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )}}
export default ManufacturerList;
