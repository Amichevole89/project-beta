import React from 'react';

class VehicleList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            models: [],
        }
    }
    async getVehicleData() {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({models: data.models})
        }
    }
    async componentDidMount() {
        this.getVehicleData()
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>

                <tbody>
                    {this.state.models.map(model =>{
                        return(
                            <tr key={model.id}>
                                <td><img alt=""className="photo" width="200px" height="auto" src={ model.picture_url }/></td>
                                <td>{ model.name }</td>
                                <td> { model.manufacturer.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }
}

export default VehicleList;