import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const AutomobileForm = () => {
  const [automobiles, setAutomobiles] = useState({
    year: "",
    color: "",
    vin: "",
    model_id: "",
    models: [],
  });

  const fetchAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const allAutomobiles = await response.json();
      setAutomobiles((s) => ({ ...s, models: allAutomobiles.models }));
    }
  };

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  const handleInputChange = (e) => {
    setAutomobiles({ ...automobiles, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...automobiles };
    delete content["models"];
    await fetch("http://localhost:8100/api/automobiles/", {
      method: "POST",
      body: JSON.stringify(content),
      headers: {
        "Content-Type": "application/json",
      },
    });

    setSuccessSubmit(true);
  };

  const [successSubmit, setSuccessSubmit] = useState(false);
  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (successSubmit) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create Automobile</h1>

            <form onSubmit={handleSubmit} className={formClass}>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={automobiles.color}
                  placeholder='Color'
                  required
                  type='text'
                  name='color'
                  id='color'
                  className='form-control'
                />
                <label htmlFor='color'>Color</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={automobiles.vin}
                  placeholder='VIN'
                  required
                  type='text'
                  name='vin'
                  id='vin'
                  className='form-control'
                />
                <label htmlFor='vin'>Vin</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={automobiles.year}
                  placeholder='Year'
                  required
                  type='number'
                  name='year'
                  id='year'
                  className='form-control'
                />
                <label htmlFor='year'>Year</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={automobiles.model_id}
                  placeholder='Model ID'
                  required
                  name='model_id'
                  id='model_id'
                  className='form-control'
                >
                  <option value=''>Choose a Model</option>
                  {automobiles.models.map((car) => {
                    return (
                      <option key={[car.id]} value={[car.id]}>
                        {car.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-between'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created Automobile!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSuccessSubmit(false)}
                  className='btn btn-outline-warning'
                >
                  Create a second automobile?
                </button>
                <Link className='btn btn-outline-primary' to='/automobiles'>
                  Return to Automobiles
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AutomobileForm;
