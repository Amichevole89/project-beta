import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const VehicleForm = () => {
  const [vehicleModels, setVehicleModels] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
    manufacturers: [],
  });
  const fetchVehicleModels = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    const allVehicleModels = await response.json();
    setVehicleModels((s) => ({
      ...s,
      manufacturers: allVehicleModels.manufacturers,
    }));
  };

  useEffect(() => {
    fetchVehicleModels();
  }, []);

  const handleInputChange = (e) => {
    setVehicleModels({ ...vehicleModels, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...vehicleModels };
    delete content.manufacturers;
    await fetch("http://localhost:8100/api/models/", {
      method: "POST",
      body: JSON.stringify(content),
      headers: {
        "Content-Type": "application/json",
      },
    });
    setSuccessSubmit(true);
  };

  const [successSubmit, setSuccessSubmit] = useState(false);
  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (successSubmit) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create Vehicle Model</h1>

            <form onSubmit={handleSubmit} className={formClass}>
              {/* {submitted ? (
            <div className='success-message'> Success!</div>
          ) : null} */}
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={vehicleModels.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={vehicleModels.picture_url}
                  placeholder='Picture URL'
                  required
                  type='text'
                  name='picture_url'
                  id='picture_url'
                  className='form-control'
                />
                <label htmlFor='pictureURL'>Picture</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={vehicleModels.manufacturer_id}
                  placeholder='Manufacturer ID #'
                  required
                  name='manufacturer_id'
                  id='manufacturer_id'
                  className='form-control'
                >
                  <option value=''>Choose a Manufacturer</option>
                  {vehicleModels.manufacturers.map((make) => {
                    return (
                      <option key={[make.id]} value={[make.id]}>
                        {make.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-between'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created new vehicle model!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSuccessSubmit(false)}
                  className='btn btn-outline-success'
                >
                  Create a second Vehicle Model?
                </button>
                <Link
                  className='btn btn-outline-primary'
                  to='/vehicles'
                  replace
                >
                  Return to Vehicle Models
                </Link>

              </div>
              <Link className='btn btn-outline-primary' to='/automobiles/'>
                  Return to Automobiles
                </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VehicleForm;
